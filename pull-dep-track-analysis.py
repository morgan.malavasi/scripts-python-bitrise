import json, collections
from datetime import datetime

import urllib2

DEPTRACK_API_KEY = 'mzelrX8KlN5nW9abhwW5DNbQ22pGoMnb'
DEPTRACK_PROJECT_UUID = 'acc41b7b-8a76-42f9-a26a-da8ba00110ed'
DEPTRACK_URL = 'http://leonardo.cloud.reply.eu/bu-ca/dep-track'
max_target_score = 100

def read_json(endpoint, key):
    request = urllib2.Request(endpoint,
                              headers = {'X-Api-Key' : key, 'Content-Type': 'application/json'})
    return json.loads(urllib2.urlopen(request).read())




def format_timestamp(ts):
    return datetime.utcfromtimestamp(int(ts) / 1000).strftime('%Y-%m-%d %X')

# read json info
project_info = read_json(DEPTRACK_URL + '/api/v1/project/' + DEPTRACK_PROJECT_UUID, DEPTRACK_API_KEY)
# read vulnerabilities
vulnerabilities = read_json(DEPTRACK_URL + '/api/v1/vulnerability/project/' + DEPTRACK_PROJECT_UUID, DEPTRACK_API_KEY)

invalid_version_deps = []
vulns = sorted(vulnerabilities, key=lambda key: (key['cvssV3BaseScore'] + 1
                                                 if 'cvssV3BaseScore' in key else key['cvssV2BaseScore'] if 'cvssV2BaseScore' in key else 1), reverse = True)
components = collections.OrderedDict()
incriminated_deps = []
max_cvss = 0

score_cvss_v2 = 0
score_cvss_v3 = 0
crit_score = 0
high_score = 0
medium_score = 0
low_score = 0

for vuln in vulns:

    vuln_score = 0

    for component in vuln['components']:
        key = '%s:%s:%s' % (component['group'] if "group" in component.keys() else '', component['name'] if "name" in component.keys() else '', component['version'] if "version" in component.keys() else '')
        if key not in components:
            components[key] = []
        components[key].append(vuln)

        if vuln['severity'] == 'CRITICAL':
            crit_score = crit_score+1
        elif vuln['severity'] == 'HIGH':
            high_score = high_score+1
        elif vuln['severity'] == 'MEDIUM':
            medium_score = medium_score+1
        else:
            low_score = low_score +1

        if 'cvssV3BaseScore' in vuln and vuln['cvssV3BaseScore'] > score_cvss_v3:
            score_cvss_v3 = vuln['cvssV3BaseScore']
        if 'cvssV2BaseScore' in vuln and vuln['cvssV2BaseScore'] > score_cvss_v2:
            score_cvss_v2 = vuln['cvssV2BaseScore']

        if 'cvssV3BaseScore' in vuln:
            vuln_score = vuln['cvssV3BaseScore']
        elif 'cvssV2BaseScore' in vuln:
            vuln_score = vuln['cvssV2BaseScore']
        if vuln_score > max_cvss:
            max_cvss = vuln_score
        if vuln_score >= max_target_score and not key in incriminated_deps:
            incriminated_deps.append(key)

last_bom_import = format_timestamp(project_info['lastBomImport']) if 'lastBomImport' in project_info else 'N/A'

f = open('dependency-track-report.html', 'w+')
f.write('''<html>
<head>
  <title>Vulnerabilities Report :: %s</title>
  <style type='text/css'>
  body { font-family: Arial, Helvetica, sans-serif; }
  thead { background-color: #000; color: #fff; }
  tbody tr:nth-child(even) { background-color: #eee; }
  a { color: black; }
  a.goup {text-decoration: none; display: block; text-align: right; }
  .circle:before { content: ' \\25CF'; font-size: 2em; }
  .upgradable { font-weight: bolder; }
  .upgradable:before { content: ' \\21AA  '; }
  .upgradable:after { content: ' \\26A0'; color: orange; font-size: 1.5em; }
  </style>
</head>
''' % project_info['name'])

f.write('<body><h1>%s</h1><h2>Vulnerabilities Report </h2><p><strong>Bom version:</strong> <em>%s</em>, ' \
        '<strong>Latest BOM Import:</strong> <em>%s</em>, <strong>Estimated CVSS:</strong> <em>%s</em>, ' % \
        (project_info['name'], project_info['lastBomImportFormat'], last_bom_import, max_cvss))

f.write('<h3>Vulnerable levels </h3>')
f.write('<h3>CRITICAL: %s</h3>' % crit_score)
f.write('<h3>HIGH: %s </h3>' % high_score)
f.write('<h3>MEDIUM: %s</h3>' % medium_score)

f.write('<a id="dependencies-list"></a><h3>Vulnerable Dependencies (%s)</h3><ul>' % (len(components)))
for component in components.keys():
    f.write('<li><a href="#%s">%s</a></li>' % (component, component))
f.write('</ul>')
if len(invalid_version_deps) > 0:
    f.write('<h3>Warning: unstable versions detected!</h3><ul>')
    for component in invalid_version_deps:
        f.write('<li>%s</li>' % component)
    f.write('</ul>')

f.write('<h3>Vulnerabilities Detail</h3>')

for component in components.keys():

    max_severity = components[component][0]['severity']
    if max_severity == 'CRITICAL':
        color = 'red'
    elif max_severity == 'HIGH':
        color = 'orange'
    elif max_severity == 'MEDIUM':
        color = 'yellow'
    else:
        color = 'green'


    f.write(('''
        <a id='%s'></a>
        <hr/><div><p><span class='circle' style='color: %s;'></span>
        <strong>GAV: </strong><em>%s</em></p>
        <table><thead><tr> 
        <th>Vulnerability</th>
        <th>Severity</th>
        <th>CWE</th>
        <th>CVSSv3 Base Score</th>
        <th>CVSSv3 Vector</th>
        <th>CVSSv2 Base Score</th>
        <th>CVSSv2 Vector</th>
        <th>Published</th>
        </tr></thead><tbody>''') % (component, color, component))
    for vuln in components[component]:
        if 'cvssV3BaseScore' in vuln:
            cvssV3 = vuln['cvssV3BaseScore']
            cvssV3Vector = vuln['cvssV3Vector']
        else:
            cvssV3 = 'N/A'
            cvssV3Vector = 'N/A'
        if 'cvssV2BaseScore' in vuln:
            cvssV2 = vuln['cvssV2BaseScore']
            cvssV2Vector = vuln['cvssV2Vector']
        else:
            cvssV2 = 'N/A'
            cvssV2Vector = 'N/A'

        if 'cwe' in vuln:
            cwe = '<strong>CWE-%s:</strong> %s' % (vuln['cwe']['cweId'], vuln['cwe']['name'])
        else:
            cwe = 'N/A'

        published = format_timestamp(vuln['published']) if 'published' in vuln else 'N/A'

        if vuln['severity'] == 'CRITICAL':
            colorField = 'red'
        elif vuln['severity'] == 'HIGH':
            colorField = 'orange'
        elif vuln['severity'] == 'MEDIUM':
            colorField = 'yellow'
        else:
            colorField = 'green'
        f.write(('''<tr> 
            <td><a href="https://nvd.nist.gov/vuln/detail/%s" target="_blank">%s</a></td>
            <td style="text-align: center;color:%s;background:grey"">%s</td>
            <td>%s</td>
            <td style="text-align: center;">%s</td>
            <td style="text-align: center;">%s</td>
            <td style="text-align: center;">%s</td>
            <td style="text-align: center;">%s</td>
            <td style="text-align: center;">%s</td>
            </tr>''') % (vuln['vulnId'], vuln['vulnId'], colorField, vuln['severity'], cwe, cvssV3, cvssV3Vector, cvssV2, cvssV2Vector, published))

    f.write('</tbody></table></div><a href="#dependencies-list" class="goup" title="Go Up">&#9650;</a>')

f.write('</body></html>')

f.close()

response = {}
response['status'] = 'ok' if max_cvss < max_target_score else 'ko'
response['invalid_cvss'] = max_cvss >= max_target_score
response['max_score'] = max_cvss
response['target_score'] = max_target_score
response['incriminated_deps'] = incriminated_deps
response['invalid_version_deps'] = invalid_version_deps

print(json.dumps(response))
