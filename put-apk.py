import requests

with open('/bitrise/deploy/app-release-bitrise-signed.apk', 'rb') as f:
    data = f.read()

response = requests.put(
    'http://leonardo.cloud.reply.eu/bu-ca/artifactory/artifactory/demo-android-app/app-release-bitrise-signed.apk',
    data=data,
    auth=('admin', 'BlueReplyC@2023'),
)
