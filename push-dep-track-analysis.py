import requests

headers = {
    # requests won't add a boundary if this header is set when you pass files=
    # 'Content-Type': 'multipart/form-data',
    'X-Api-Key': 'mzelrX8KlN5nW9abhwW5DNbQ22pGoMnb',
}

files = {
    'autoCreate': (None, 'true'),
    'project': (None, 'acc41b7b-8a76-42f9-a26a-da8ba00110ed'),
    'bom': open('/bitrise/src/app/build/reports/bom.xml', 'rb'),
}

response = requests.post('http://leonardo.cloud.reply.eu/bu-ca/dep-track/api/v1/bom', headers=headers, files=files)
print(response)
